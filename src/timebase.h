/*
 * timebase.h
 *
 *  Created on: 16.09.2018
 *      Author: googy
 */

#ifndef TIMEBASE_H_
#define TIMEBASE_H_

void timer0_init();
void stoptimer_init(uint32_t* counter);
uint8_t stoptimer_expired(uint32_t* counter, uint16_t ms_time);
uint8_t stoptimer_expired_sec(uint32_t* counter, uint8_t s_time);

#endif /* TIMEBASE_H_ */
