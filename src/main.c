
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>

#include <util/delay.h>
#include <stdio.h>
#include <inttypes.h>

#include <stdlib.h>	// itoa

#include "mcp2515.h"
#include "global.h"
#include "defaults.h"

#include "uart.h"
#include "timebase.h"


// Energiequellen
enum sources_t{
	HOLZ,				// Atmos Holzkessel
	HEIZOEL,			// Buderus Heizölkessel
	UNBEKANNT,			// aktuelle Position nicht bekannt
	TURNING				// Ventil befindet sich in Bewegung
};

tCAN message, rMessage;
char buf[15];
uint8_t soll = HOLZ;
uint8_t ist = UNBEKANNT;
uint8_t turn = UNBEKANNT;
uint32_t send_timer = 0;
uint32_t source_timer = 0;

void mischer_SM() {
	// Energiequelle
	if (ist != soll) {				// Mischer nicht in Sollposition oder unbekannt
		/* ist Zustand wird zu TURNING geändert daher werden eine nachträgliche Änderung berücksichtigt
		   auch wenn sich soll ändert, denn soll wird extern nie auf TURNING gestellt
		   daher bis die Endposition erreicht ist wird diese Stelle immer erreicht
		*/
		if (ist != TURNING || soll != turn) {       // Es wird noch keine Drehung ausgeführt oder Drehrichtung hat sich geändert
            // vorsichtshalber Drehung abschalten, vor allem interessant für die Initialisierung des Mischers
            PORTB &= ~(1 << PB1);                   // Spannung wegschalten
            PORTB &= ~(1 << PB0);                   // Drehrichtung reset
			if (soll == HOLZ) {
                PORTB &= ~(1 << PB0);               // Drehrichtung Holz
			} else if (soll == HEIZOEL) {
                PORTB |= (1 << PB0);                // Drehrichtung Heizöl
			}
            PORTB |= (1 << PB1);                    // Spannung zuschalten
            stoptimer_init(&source_timer);          // initialisiere Drehtimer
			ist = TURNING;							// ändere Zustand, Mischer befindet sich in Bewegung
			turn = soll;			                // notiere Drehrichtung
			/* auch hier kann es keine Inkonsistenzen geben, dann dieser Teil wird in einem Durchlauf ausgeführt
			   und erst nach dem die Drehrichtung notiert wurde kann soll sich ändern,
			   dies ist auch kein Problem, denn soll wird solange ignoriert bis die Drehung abgeschlossen ist.
			*/
		}

		if (stoptimer_expired_sec(&source_timer, 120) && ist == TURNING) {	// warte bis Drehung abgeschlossen
			PORTB &= ~((1 << PB0) | (1 << PB1));							// Mischer abschalten, da fertig gedreht
			ist = turn;				// notiere neue Position
		}
	}
}

int main(void) {
    // init Ports
    PORTB &= ~((1 << PB0) | (1 << PB1));
    DDRB |= (1 << PB0) | (1 << PB1);

    timer0_init();  // Zeitbasis
    uart_init();    // debug Nachrichten

    DDRB |= 1 << PB2;		// !!!!!!!!!!!!!!!!!!!!! SS to output, sonst keine SPI Kommunikation
    // Versuche den MCP2515 zu initilaisieren
    if (!mcp2515_init()) {    // !!!! Filter auf 0x2A eingestellt !!!!
        uart_puts("MCP2515 FAIL\n\r");
        return 0;
    } else {
        uart_puts("MCP2515 OK\n\r");
    }

    stoptimer_init(&send_timer);

    while (1) {
        if (stoptimer_expired_sec(&send_timer, 1)) {
            uart_puts("send timer expired, broadcast state\n\r");
        }
        _delay_ms(500); // TODO delay entfernen und auf timer umstellen wie zwei Zeilen darüber
        // prepare message
        message.id = 0x2B;
        message.header.rtr = 0;
        message.header.length = 8;
        message.data[0] = ist;  // 
        message.data[1] = soll;
        message.data[2] = turn;
        message.data[3] = 0;
        message.data[4] = 0;
        message.data[5] = 0;
        message.data[6] = 0;
        message.data[7] = 0;

        // send message
        if (mcp2515_send_message(&message)) {
            //uart_puts("Nachricht konnte gesendet werden\n\r");
        } else {
            uart_puts("ERROR Nachricht konnte nicht gesendet werden\n\r");
        }

        // check for incoming messages
        // !!!! Filter auf 0x2A eingestellt !!!!
        while (mcp2515_check_message()) {		// Nachricht vorhanden
            if (mcp2515_get_message(&rMessage)) {		// Nachricht erfolgreich angekommen
                // uart_puts("ID: ");
                // itoa((rMessage.id), buf, 16);
                // uart_puts(buf);
                // uart_puts("   DLC: ");
                // itoa((rMessage.header.length), buf, 10);
                // uart_puts(buf);
                // uart_puts("   Data: ");
                // for (uint8_t i = 0; i < rMessage.header.length; i++) {
                //     itoa((rMessage.data[i]), buf, 16);
                //     uart_puts(buf);
                //     uart_putc(' ');
                // }
                // uart_puts("\n\r");

                if (rMessage.id == 0x2A) {    // redundant, da Filter bereits auf ID 0x2A eingestellt
                    soll = rMessage.data[0];
                }
            } else {
                uart_puts("FAIL: unerwartet keine Nachricht mehr im Buffer vorhanden\n\r");
            }
        }
        mischer_SM();
    }

    return 0;
}
