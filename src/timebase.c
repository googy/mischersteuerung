/*
 * timebase.c
 *
 *  Created on: 16.09.2018
 *      Author: googy
 */
#include <avr/io.h>
#include <avr/interrupt.h>
#include "timebase.h"

#include "uart.h"
#include <stdlib.h>	// itoa

volatile uint32_t ticks = 0;

// Timer 0 als Zeitbasis, Frequenz???
void timer0_init() {
	TCCR0A = (1 << WGM01);				// kein PWM, CTC mode, bis OCR0A dann reset
	TCCR0B = (1 << CS02);				// kein PWM, prescaler 256, 31,250kHz bei 8MHz
	OCR0A = 125;						// bis OCR0A z�hlen, dann reset
	TIMSK0 |= (1 << OCIE0A);			// Output Compare A Match Interrupt
}

// 250Hz Timer Interrupt (ungenau, nicht Uhr-fähig)
ISR(TIMER0_COMPA_vect) {
	ticks++;
}

// speichere aktuelle Zeit in Ticks im Counter
void stoptimer_init(uint32_t* counter) {
    cli();
    *counter = ticks;
    sei();
}

// wenn die aktuelle Zeit den gespeicherten Startwert + offset übersteigt return true
uint8_t stoptimer_expired(uint32_t* counter, uint16_t ms_time) {
    uint8_t ergebnis = 0;
    cli();
    if (ticks > (*counter + (ms_time >> 2))) ergebnis = 1;
    sei();
    return ergebnis;
}

// wenn die aktuelle Zeit den gespeicherten Startwert + offset übersteigt return true
uint8_t stoptimer_expired_sec(uint32_t* counter, uint8_t s_time) {
    uint8_t ergebnis = 0;
    cli();
    //if (ticks > (*counter + (s_time * 250))) ergebnis = 1;
    if (ticks > (*counter + ((uint32_t) s_time << 8))) ergebnis = 1;    // ungenauer als vorhrige Zeile, da "<< 8" entspricht "* 256" und nicht "* 250"
    sei();
    return ergebnis;
}
//-----------------------------------------------------------------------------------------------------

